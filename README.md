JOpenFlow is a Java implementation of low-level OpenFlow protocol datagram
marshalling/unmarshalling and IO operations. This code base is based on
a port of openflowj (http://bitbucket.org/openflowj/openflowj) to
OpenFlow v1.3.3. 


# Compilation
Building requires Maven 2.x+ (http://maven.apache.org/). To build run
./dist.sh. Results will be placed in the dist subfolder.

Alternatively, you can use Eclipse with Maven integration and import the
project as an Existing Maven Install. Once loaded on Eclipse, you can
Run the example/SimpleController application. It is capable of doing
simple L2 MAC learning switch.


# Implementation notes
1. **New Matching and set-field action**: As required by OF1.3, this release
has a new matching function because the Standard OF1.1 match is being
depecated and all matching is moved to OXM format. This is true for the
header rewriting actions too, i.e., OFActionSetField takes prominence
for all header writing. Each field is an object of OFOXMField. If you
need to specify mask values, then use OFMatchField that inherits from
OFOXMField.

2. **Convenience functions for openflowj apps**: This release also provides
several convenience functions to improve portability of your existing
openflowj-based application to jopenflow. Most of these functions are in
OFMatch class so that older ways of setting match fields will still
work. However, in OF1.3.3, wildcarding is implicit in the OFMatch and
not needed to be specified. Since several applications still use the
OFMatch.setWildcard() to specifiy what fields are to be ignored in a
fully-populated OFMatch, we provide a related function called
setNonWilcard(Set<OFOXMField>) that removes all match fields except
those specified in that function call.

3. **ComputeLength**: In previous openflowj, the length of message
structs were calculated by the application. This is avoided in JOpenFlow
where the lengths are computed when any set() operation is performed.
Before dispatching an OFMessage, it is required to call
msg.computeLength() to update the length. THis is essential because many
of the messages are of variable length and some do not count padding.

# Release notes
- Feature complete for all of the basic OF1.3 features
- However, many of the experimenter/vendor extended features are missing
- Supporting ByteBuffer and not ChannelBuffer.

# Authors
 - Srini Seetharaman (srini.seetharaman@gmail.com)

   Original authors of openflowj for OF v1.0.3:
    - David Erickson (daviderickson@cs.stanford.edu)
    - Rob Sherwood (rob.sherwood@stanford.edu)