#!/bin/bash
set -x
TMP=`mktemp -d`
CWD=`pwd`

VERSION=`grep -m 1 version pom.xml`
if [[ $VERSION =~ ^.*\<version\>([0-9.]*)\<\/version\>.*$ ]]; then
    VERSION=${BASH_REMATCH[1]}
    echo "$VERSION"
else
    echo "Failure determining version"
    exit 1
fi

mvn clean package

# Delete any existing archives
rm -rf ${CWD}/dist
mkdir -p ${CWD}/dist

cp -ar ${CWD}/target/jopenflow13-${VERSION}.jar ${CWD}/dist
cp -ar ${CWD}/target/jopenflow13-${VERSION}-sources.jar ${CWD}/dist

mkdir -p ${TMP}/jopenflow13-${VERSION}
cp -ar * ${TMP}/jopenflow13-${VERSION}
cp -ar .classpath .settings .project ${TMP}/jopenflow13-${VERSION}
tar -czv -f ${CWD}/dist/jopenflow13-${VERSION}-source.tar.gz --exclude-vcs --exclude="jopenflow13-${VERSION}/dist" --exclude="jopenflow13-${VERSION}/ext" --exclude="**/target" --exclude="**/bin" -C ${TMP} jopenflow13-${VERSION}

mvn javadoc:jar
cp -ar ${CWD}/target/apidocs dist/
cp -ar ${CWD}/target/jopenflow13-${VERSION}-javadoc.jar ${CWD}/dist

rm -rfd ${TMP}

exit
